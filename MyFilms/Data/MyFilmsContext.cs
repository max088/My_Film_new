﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MyFilms.Models
{
    public class MyFilmsContext : DbContext
    {
        public MyFilmsContext (DbContextOptions<MyFilmsContext> options)
            : base(options)
        {
        }

        public DbSet<MyFilms.Models.Film> Film { get; set; }
    }
}
